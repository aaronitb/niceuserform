package cat.itb.niceuserform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button button_login;
    Button button_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_login = findViewById(R.id.button_login);
        button_login.setOnClickListener(this);
        button_register = findViewById(R.id.button_register);
        button_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_login){
            Intent i  = new Intent(MainActivity.this, LoginScreen.class);
            startActivity(i);
        }
        if (v.getId() == R.id.button_register){
            Intent i  = new Intent(MainActivity.this, RegisterScreen.class);
            startActivity(i);
        }
    }
}
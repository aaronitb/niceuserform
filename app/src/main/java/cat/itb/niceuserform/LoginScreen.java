package cat.itb.niceuserform;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener{
    Button button_login;
    Button button_register;

    TextInputLayout inputLayoutUsername;
    TextInputEditText inputEditTextUsername;
    TextInputLayout inputLayoutPassword;
    TextInputEditText inputEditTextPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        button_login = findViewById(R.id.button_login_login);
        button_login.setOnClickListener(this);
        button_register = findViewById(R.id.button_register_login);
        button_register.setOnClickListener(this);

        inputLayoutUsername = findViewById(R.id.input_username_login);
        inputEditTextUsername = findViewById(R.id.input_username_text_login);
        inputLayoutPassword = findViewById(R.id.input_password_login);
        inputEditTextPassword = findViewById(R.id.input_password_text_login);

    }

    @Override
    public void onClick(View v) {
        String username = inputEditTextUsername.getText().toString();
        String password = inputEditTextPassword.getText().toString();
        if (v.getId() == R.id.button_login_login){
            if (username.isEmpty()){
                inputLayoutUsername.setError("required field");
            }
            if(password.length()<8){
                inputLayoutPassword.setError("password must have at least 8 characters");
            }
            if (!username.isEmpty() && password.length()>=8){
                Intent i  = new Intent(LoginScreen.this, WelcomeScreen.class);
                startActivity(i);
            }
        }
        if (v.getId() == R.id.button_register_login){
            Intent i  = new Intent(LoginScreen.this, RegisterScreen.class);
            startActivity(i);
        }
    }
}

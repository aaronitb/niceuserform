package cat.itb.niceuserform;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class RegisterScreen extends AppCompatActivity implements View.OnClickListener {
    TextInputLayout inputLayoutUsername;
    TextInputEditText inputEditTextUsername;
    TextInputLayout inputLayoutPassword;
    TextInputEditText inputEditTextPassword;
    TextInputLayout inputLayoutRepeatPassword;
    TextInputEditText inputEditTextRepeatPassword;
    TextInputLayout inputLayoutEmail;
    TextInputEditText inputEditTextEmail;
    TextInputLayout inputLayoutName;
    TextInputEditText inputEditTextName;
    TextInputLayout inputLayoutSurnames;
    TextInputEditText inputEditTextSurnames;

    Button date;
    boolean dateSet=false;

    Spinner gender;
    ArrayAdapter<CharSequence> adapter;

    CheckBox checkBox;

    Button button_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        inputLayoutUsername = findViewById(R.id.input_username_register);
        inputEditTextUsername = findViewById(R.id.input_username_text_register);
        inputLayoutPassword = findViewById(R.id.input_password_register);
        inputEditTextPassword = findViewById(R.id.input_password_text_register);
        inputLayoutRepeatPassword = findViewById(R.id.input_repeatpassword_register);
        inputEditTextRepeatPassword = findViewById(R.id.input_repeatpassword_text_register);
        inputLayoutEmail = findViewById(R.id.input_email_register);
        inputEditTextEmail = findViewById(R.id.input_email_text_register);
        inputLayoutName = findViewById(R.id.input_name_register);
        inputEditTextName = findViewById(R.id.input_name_text_register);
        inputLayoutSurnames = findViewById(R.id.input_surnames_register);
        inputEditTextSurnames = findViewById(R.id.input_surnames_text_register);

        date = findViewById(R.id.button_date_register);
        date.setOnClickListener(this);

        gender = findViewById(R.id.spinnerGender);
        adapter = ArrayAdapter.createFromResource(this,R.array.spinnerItems, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);

        checkBox = findViewById(R.id.is_justified);

        button_register = findViewById(R.id.button_register_register);
        button_register.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String username = inputEditTextUsername.getText().toString();
        String password = inputEditTextUsername.getText().toString();
        String repeatPassword = inputEditTextUsername.getText().toString();
        String email = inputEditTextUsername.getText().toString();
        String name = inputEditTextUsername.getText().toString();
        String surnames = inputEditTextUsername.getText().toString();

        switch (v.getId()) {
            case R.id.button_date_register:
                setDate(v);
                break;
            case R.id.button_register_register:
                if (!checkBox.isChecked()){
                    checkBox.setError("You must check this");
                    checkBox.requestFocus();
                }
                if (!dateSet){
                    date.setError("Select a date");
                    date.requestFocus();
                }
                if (username.isEmpty()){
                    inputLayoutUsername.setError("required field");
                }
                if (password.length()<8){
                    inputLayoutPassword.setError("password must have at least 8 characters");
                }
                if (repeatPassword.equals(password)){
                    inputLayoutRepeatPassword.setError("passwords must match");
                }
                if (email.isEmpty()){
                    inputLayoutEmail.setError("required field");
                }
                if (name.isEmpty()){
                    inputLayoutName.setError("required field");
                }
                if (surnames.isEmpty()){
                    inputLayoutSurnames.setError("required field");
                }
                if (checkBox.isChecked() && dateSet && !username.isEmpty() && !password.isEmpty() && repeatPassword.equals(password) && !email.isEmpty() && !name.isEmpty() && !surnames.isEmpty()){
                    Intent i = new Intent(RegisterScreen.this, WelcomeScreen.class);
                    startActivity(i);
                    break;
                }
        }
    }

    public void setDate(View v) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(RegisterScreen.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthofyear, int dayofmonth) {
                date.setText(dayofmonth + "-" + (monthofyear+1) + "-" + year);
                dateSet = true;
            }
        }, year, month, day);
        dpd.show();
    }
}
